{
while ! nc -z mongo1 27017; do
  sleep 0.1 # wait for 1/10 of the second before check again
done &&
mongosh --port 27017 -- "$MONGO_INITDB_DATABASE" 2>&1 > /data/logs/mongo-init.log <<-EOJS
  var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
  var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
  db = (new Mongo("mongo1:27017"))
  admin.auth(rootUser, rootPassword);
EOJS && sleep 3 &&
mongosh --port 27017 -- "$MONGO_INITDB_DATABASE" 2>&1 > /data/logs/mongo-rs-init.log <<-EOJS
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
	var dbInit  = 'admin';
    db = (new Mongo("mongo1:27017")).getDB(dbInit);
    db.auth(rootUser, rootPassword);
    config = {
      "_id" : "rs0",
      "members" : [
        {
          "_id" : 0,
          "host" : "mongo1:27017"
        }
      ]
      };
    rs.initiate(config);
EOJS
} &

