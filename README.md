# MongoDB_Transaction_DockerFile_and_compose



Packing together some useful resource found around "The Internet" to get a docker image of single mongodb instance and mongo-express image supporting transaction (docker-compose)

To get a fresh replica.key file I've typed this command from linux shell:
`openssl  rand -base64 756 > ./replica.key`

## Getting started

just type 
`docker compose up`
and everything'd just be fine (it you have docker installed on your system)

if mongo express won't start you should add "restart always" in docker compose. (or manually start it )

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/KalibaDev/mongodb_transaction_dockerfile_and_compose.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/KalibaDev/mongodb_transaction_dockerfile_and_compose/-/settings/integrations)

## Collaborate with your team
Societies are fine only with an odd number of people in it, and three is too much.
Just kidding :p

## Name
"no name" project seems an ugly name to me, So here we are.

## Description
Receipe to build a docker image of a mongo db supporting transactions (configuring a replica, to be tested) 

## Visuals
to do

## Installation
to do

## Usage
to do

## Support
to do

## Roadmap
to do

## Contributing
to do

## Authors and acknowledgment
Thank to all Guys on the internet who wrote something about the replica issue on transactions with mongo db zooming on the solution :) 

## License
MIT

