FROM mongo:latest
COPY replica.key /data/replica.key
RUN mkdir -p /data/logs && chmod 777 -R /data/logs
RUN chmod 400 /data/replica.key
RUN chown 999:999 /data/replica.key
RUN apt-get update && apt-get install netcat -y
RUN apt-get install -y curl 
COPY init.sh /docker-entrypoint-initdb.d/init.sh
EXPOSE 15672